# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem			1.0

name				epics-probe
version				1_1_8_0
revision			1
categories			science
platforms			darwin
maintainers			ligo.org:jonathan.hanks					
license				EPICS
description			EPICS Probe
long_description	Probe is a Motif application that can monitor one PV and display information about the underlying database record.
depends_lib			port:epics-base
depends_build		port:epics-base
homepage			http://www.aps.anl.gov/epics/

set extensions_name	extensions_${name}
# additional component names
set top_ver			20120904
# where it is going to live
set dest_prefix /src/epics
set epicsdir ${destroot}${prefix}${dest_prefix}
# list of extensions
set extensions [list probe${version}]

distfiles			extensionsTop_${top_ver}.tar.gz \
					probe${version}.tar.gz

master_sites		http://www.aps.anl.gov/epics/download/extensions/

checksums			extensionsTop_${top_ver}.tar.gz \
					rmd160	7a10608d9a053d93fa5833e2c9bf1dc5f71f2dbf \
					sha256	5f2f3953c3bffba735d6b57614fd05068b1295b28ed55fd1e1b85157dbc5808d \
					probe${version}.tar.gz \
					rmd160	1a5374806aef075c05ef7dbae42b55aeaeff8424 \
					sha256	5f58a6f2046180cbbb97e9a1237eeb2f05a0b833975e390cc680d0fb441f271b

worksrcdir			epics
extract.mkdir		yes

depends_lib			port:readline \
					port:openmotif
patch.pre_args		-p1
patchfiles			patch-0001-Change-paths-to-use-macports-X11-compontents.diff

build.cmd			gnumake
build.env			EPICS_HOST_ARCH=darwin-x86

use_configure		no

post-extract {
	foreach extension $extensions {
		move ${workpath}/epics/${extension} ${workpath}/epics/extensions/src
	}
	copy ${filespath}/makefile ${workpath}/epics/makefile
	system -W ${workpath}/epics "ln -s ${prefix}${dest_prefix}/base base"
}

post-patch {
	set fp [open ${workpath}/epics/makefile a]
	foreach extension ${extensions} {
		puts ${fp} "\tgnumake -C ${extensions_name}/src/${extension} -j1"
	}
	close $fp
	reinplace	"s|@@EXTENSIONS@@|${extensions_name}|" ${workpath}/epics/makefile
	reinplace	"s|@@PREFIX@@|${prefix}|" ${workpath}/epics/extensions/configure/os/CONFIG_SITE.darwin-x86.darwin-x86
	move ${workpath}/epics/extensions ${workpath}/epics/${extensions_name}
}

destroot {
	file mkdir ${epicsdir}
	system -W ${workpath}/epics "cp -a ${extensions_name} ${epicsdir}"
}

post-destroot {
	set ext_root ${epicsdir}/${extensions_name}
	set ext_bin ${ext_root}/bin
	set ext_src ${ext_root}/src

	# build a list of areas where binaries or libraries that need some path rewritting might live at
	set patterns [list ${ext_bin}/darwin-x86/*]
	set patterns [lappend patterns ${ext_src}/*/O.darwin-x86/*]
	#set patterns [lappend patterns ${ext_src}/*/*/O.darwin-x86/*]

	# use the install_name_tool application to rewrite paths since epics hardcodes path information
	foreach pattern $patterns {
		#puts "Checking ${pattern} for rewritting"
		foreach fname [glob ${pattern} ] {
			if {[file isfile ${fname}] != 1} {
				continue
			}
			if {[catch {file link ${fname}}] == 0} {
				continue
			}
			if {[catch {exec otool -L ${fname}}] != 0} {
				continue
			}
			set lines [exec otool -L ${fname}]

			foreach line [split ${lines} "\n"] {
				set line [string trim ${line}]
				if {[string first ".dylib" ${line}] < 0} {
					continue
				}
				set line [string range ${line} 0 [string first ".dylib" ${line}]+5]
				if {[string first ${workpath}/epics ${line}] != 0} {
					continue
				}

				set updated ${prefix}${dest_prefix}[string range ${line} [string length ${workpath}/epics] [string length ${line}]]
				#puts "install_name_tool -change ${line} ${updated} ${fname}"
				exec install_name_tool -change ${line} ${updated} ${fname}
			}
		}
	}

	# link components into the file system in the appropriate places
	foreach pattern [list ${ext_bin}/darwin-x86/*] {
		foreach fname [glob ${pattern}] {
			set rel_path [string range ${fname} [string length ${destroot}${prefix}] [string length ${fname}]]
			#puts "ln -s ..${rel_path} [file tail $fname]"
			system -W ${destroot}${prefix}/bin "ln -s ..${rel_path} [file tail $fname]"
		}
	}
}
